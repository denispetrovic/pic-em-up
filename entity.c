#include "entity.h"

void entity_display(t_entity* entity, char type, char state, char sprite, char pattern ,char offset_movement, char witness, char posx, char posy)
{
    entity->type = type;
    entity->state = state;
    entity->sprite = sprite;
    entity->pattern = pattern;
    entity->offset_movement = offset_movement;
    entity->witness = witness;
    entity->posx = posx;
    entity->posy = posy;
}

void entity_randomize(t_entity* entity)
{
    entity->type = 0;
    entity->state = 1;
    entity->sprite=3;
    switch(rand()%3){
        case 0: entity->pattern = '1';
            break;
        case 1: entity->pattern = '2';
            break;
        case 2: entity->pattern = '7';
            break;    }
    entity->offset_movement = 0;
    entity->witness = 0;
    entity->posx = rand(1)%120+10;
    entity->posy = rand(1)%50+5;
    
}
void entity_randomize2(t_entity* entity)
{
    entity->type = 0;
    entity->state = 1;
    entity->sprite=3;
    switch(rand()%3){
        case 0: entity->pattern = '1';
            break;
        case 1: entity->pattern = '2';
            break;
        case 2: entity->pattern = '7';
            break;    }
    entity->offset_movement = 0;
    entity->witness = 0;
    entity->posx = rand(1)%60+5;
    entity->posy = rand(1)%50+15;
    
}

/*char entity_init(t_entity* e)
{
    return display_bmp_sprite(e->sprite, e->posx, e->posy);
}*/

char check_if_free_space(t_entity* entity[]){
    int i;
    for (i=0;i<MAX_ENTITIES_ON_SCREEN;i++){
        if(entity[i]==0x00){
            return i;//If we find a free space, we return its index
        }
    }
    return -1;//If there is no free space, we return -1
}