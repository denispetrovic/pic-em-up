/* 
 * File:   init.h
 * Author: Adrian Chazottes
 *
 * Created on 18 novembre 2016, 10:00
 */

#ifndef INIT_H
#define	INIT_H

// --- microchip define file for your PIC
//#include <xc.h>
//#include <p18f4550.h>

#define TOTAL_ENEMIES_AND_WALLS 30
#define MAX_ENTITIES_ON_SCREEN 20
// declare the function prototypes

// inits the PIC18F device
//void initMyPIC18F(void);


#endif	/* INIT_H */

