#include "init.h"
#include "game.h"

// --- init the PIC18F device
void initMyPIC18F(void)
{
	// PORTA digital
	ADCON1 = 0x0F ;
	ADCON0 = 0;

	// set all ports as OUTPUTS
	TRISA = 0x00;
	TRISB = 0x00;
	TRISC = 0x00;
	TRISD = 0x00;
	TRISE = 0x00;

	// set port by port on "all zeros"
	PORTA = 0x00;
	PORTB = 0x00;
	PORTC = 0x00;
	PORTD = 0x00;
// make sure to have an empty LAST line in any *.c file (just hit an Enter)!

	PORTE = 0x00;

}
// make sure to have an empty LAST line in any *.c file (just hit an Enter)!

void createStruct(t_game *game_t) {
    int i;
    game_t->state=1;//Alive
    game_t->score=0;
    game_t->movement='n';//No movement at first
    
    for (i=0;i<MAX_ENTITIES_ON_SCREEN;i++){
        game_t->entities[i]=0;//We point to a random but well defined location so that it's like a "NULL"
    }
    for (i=0;i<TOTAL_ENEMIES_AND_WALLS;i++){
        game_t->script[i].cycle_number=1000+i*1000;
        game_t->script[i].nb_of_enemies_in_group=4;
        game_t->script[i].entity->type=0;//Enemies
        game_t->script[i].entity->state=1*i;//The number of lives of the enemies. It gets progressively harder
        game_t->script[i].entity->sprite=3;//To define
        if(i%2) {
            game_t->script[i].entity->pattern=7;
            game_t->script[i].entity->posx=i%20;
            game_t->script[i].entity->posy=30+(i%10);
            game_t->script[i].entity->sprite=4;//To define

        } else if(i%3) {
            game_t->script[i].entity->pattern=1;
            game_t->script[i].entity->posx=50+i%9;
            game_t->script[i].entity->posy=i%35;
            game_t->script[i].entity->sprite=5;//To define

        } else if(i%5) {
            game_t->script[i].entity->pattern=2;
            game_t->script[i].entity->posx=70+i%5;
            game_t->script[i].entity->posy=40+i%20;
        }
        game_t->script[i].entity->offset_movement=0;
    }
}
