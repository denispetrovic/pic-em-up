/* 
 * File:   main.h
 * Author: Denis
 *
 * Created on 18 novembre 2016, 10:00
 */

#ifndef MAIN_H
#define MAIN_H


#include <stdio.h>
#include <stdlib.h>
#include <xc.h>
#include "glcd.h"
#include "init.h"
#include "display.h"
#include "movement.h"
#include "menu.h"


#define RS  RC0
#define RW RC1
#define EN  RC2
#define CS1 RC3
#define CS2 RC4
#define RST RC5
#define data PORTB


#endif	/* MAIN_H */

