// /* 
//  * File:   movement.h
//  * Author: Carlos
//  *
//  * Created on 2 decembre 2016, 22:23
//  */
#ifndef GAME_H
#define	GAME_H

#include "movement.h"
#include "init.h"

typedef struct ennemy_arrival_script {
    int cycle_number;
    char nb_of_enemies_in_group;
    t_entity  *entity;
}t_ennemy_arrival_script;
typedef struct game {
    char state;//Alive or dead (or maybe see if we add lives)
    char score;
    char movement;//The next movement of the player
    //char shooting;
    //char shooting_type;
    t_entity *entities[MAX_ENTITIES_ON_SCREEN];//Array of pointers on struct
    //char nb_of_entities;
    //char *time;
    t_ennemy_arrival_script  script[30];
}t_game;

char game_loop(t_game *game);
 void createScriptStruct(t_game *game_t);


// //Main game loop
// char game_loop(t_game *	game);
    
#endif