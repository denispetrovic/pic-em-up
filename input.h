/* 
 * File:   input.h
 * Author: Florian
 *
 * Created on 7 d�cembre 2016, 23:57
 */

#ifndef INPUT_H
#define	INPUT_H

#ifdef	__cplusplus
extern "C" {
#endif

#include <xc.h>
#include "init.h"
#include "game.h"

void interrupt   tc_int  (void);

#ifdef	__cplusplus
}
#endif

#endif	/* INPUT_H */

