#include <xc.h>
#include "main.h"
#include "game.h"

#pragma config FOSC = HS 	//oscillator HS
#pragma config PWRT = OFF
#pragma config BOR = OFF
#pragma config WDT = OFF 	//Disables watchDog
#pragma config LVP = OFF 	//Disables low voltage programming
#pragma config DEBUG = ON	//Debugs ON
#pragma CONFIG MCLRE  = ON 

void main (void)
{
    // Initialisation
   //Init port
    t_game play;
//    char addr_play = &play;
 //   play.state = 1; //Alive and playing
 //   play.score = 0;
//    t_entity player;
//    t_entity random;
//
//    player.state=1;
//    player.sprite=1;
//    player.posx=20;
//    player.posy=30;
//    player.type=1;
//    player.pattern = '1';
//    player.witness = 0;
//    player.offset_movement=0;
//    
//
//    random.state=1;
//    random.posx=80;
//    random.posy=30;
//    random.pattern = '7';
//    random.offset_movement=0;
//    random.witness=0;
          
    
    
   	ADCON1 = 0x0F;
//	TRISAbits.TRISA0 = 1;
    TRISE = 0xFF;
    TRISA = 0x00;
    PORTA = 0;
    PORTC = 0;
	TRISB = 0;				//PORTB output
	PORTB = 0; 				//Reset PORTB


	TRISD = 0xFF;			//PORTD input
	PORTD = 0; 				//Reset PORTD

//	TRISC = 0xff;			//PORTC input

    glcd_Init(1);//Turn the GLCD on
    glcd_FillScreen(0);	
   //display_bmp_string("H",20,10);  
    //display_glcd();
    char choice = 0;
    char i;

    // Main menu loop
	while(1)
	{
  
        menu_loop() ;
        //display_bmp_string("COUCOU\nCOMMENT\nOK",50,30);
        display_glcd();

        //glcd_Image();
        if(PORTEbits.RE0 == 1)
        {
            display_bmp_fill(0);
            game_loop(&play);
            choice = 1;
        }
        if(PORTEbits.RE1 == 1)
        {
            //glcd_FillScreen(0);
            //glcd_FillScreen(0) ;
            display_bmp_fill(0);
            choice = 2;
        }
        if(PORTEbits.RE2 == 1)
        {
            //glcd_FillScreen(0) ;
            display_bmp_fill(0);
            //glcd_FillScreen(0);
            choice = 3;
        }
       
        while(choice == 1)
        {
            game_loop(&play) ;
            choice=0;
        }

        // help menu
        while(choice == 2)
        {
            help() ;
            display_glcd();
            
            if(PORTEbits.RE1 == 1)
            {    
                display_bmp_fill(0);
                choice = 0 ;
               // glcd_FillScreen(0) ;	
            }
        }
        
        // credits display
        while(choice == 3)
        {
            credits() ;
            display_glcd();

            if(PORTEbits.RE2 == 1)
            {    
                display_bmp_fill(0);            
                choice = 0 ;
               // glcd_FillScreen(0) ;
            }
        }

        
        //movement_pattern(&player);
       /* movement_pattern(&random);
       // glcd_PlotPixel(player.posx,player.posy,255);
        //random.posx++;
        display_bmp_sprite(3,random.posx,random.posy);
        display_glcd();
           __delay_ms(80);    
           __delay_ms(80);
        // glcd_FillScreen(0);
        display_bmp_fill(0);
*/
	}
}

