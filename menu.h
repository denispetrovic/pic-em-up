#ifndef MENU_H
#define MENU_H

#include "glcd.h"
#include "display.h"

// displays the menu
void menu_loop(void) ;
void credits(void) ;
void help(void) ;
void gg_victory(void) ;
void try_again(void) ;

#endif	/* MENU_H */
