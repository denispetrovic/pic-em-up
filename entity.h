/* 
 * File:   entity.h
 * Author: Joseph
 *
 * Created on 6 d�cembre 2016, 13:52
 */

#ifndef ENTITY_H
#define	ENTITY_H

#include "display.h"
#include "init.h"

typedef struct movement {
	char* movement; // Donn�es du mouvement
	
}t_movement;

typedef struct entity {
	char type; // Tir ennemi/alli�, d�cor, ennemi, joueur
	char state; // Mort/Vivant, ou peut �tre vies?
	char sprite; // index of the sprite
	char pattern; // pointeur sur le pattern du mouvement
	char offset_movement; // Utilis� pour le mouvement
        char witness;
	char posx, posy; // Position

}t_entity;

void entity_init(t_entity* entity, char type, char state, char sprite, char offset_movement, char witness, char posx, char posy);
void entity_randomize(t_entity* entity);


// Display the sprite of the entity on its position
// Returns 0 if no collision, 1 if collision
void entity_display(t_entity* entity, char type, char state, char sprite, char pattern ,char offset_movement, char witness, char posx, char posy);

//Checks if the array that displays enemies on the screen is not full
// If there is free space, return the index of the first free space
char check_if_free_space(t_entity** entity);
void entity_randomize2(t_entity* entity);



#ifdef	__cplusplus
extern "C" {
#endif




#ifdef	__cplusplus
}
#endif

#endif	/* ENTITY_H */

