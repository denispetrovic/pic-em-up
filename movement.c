/* 
 * File:   movement.c
 * Author: Erwan
 *
 * Created on 3 d�cembre 2016, 22:15
 */

#include "movement.h"

char pattern7(char index) 
{
    switch (index)
    {
        case 0 : return 0xB;
        case 1 : return 0xB;
        case 2 : return 0xB;
        case 3 : return 0xB;
        case 4 : return 0x9;
        case 5 : return 0x9;
        case 6 : return 0x9;
        case 7 : return 0x9;
        case 8 : return 0xF;
        case 9 : return 0xF;
        case 10 : return 0xF;
        case 11 : return 0xF;
        case 12 : return 0xD;
        case 13 : return 0xD;
        case 14 : return 0xD;
        case 15 : return 0xD;//8185C2C5
        default : break;
    }
}

char pattern3(char index) //RIGHT
{
    return 0x5;
}
char pattern4(char index) //LEFT
{
    return 0x7;
}
char pattern5(char index) //LEFT + UP
{
    return 0xF;
}
char pattern6(char index) //LEFT + DOWN
{
    return 0xB;
}


char pattern2(char index)
{
    switch (index)
    {
        case 0 : return 0x7;
        case 1 : return 0xA;
        case 2 : return 0x1;
        case 3 : return 0xA;
        case 4 : return 0x7;
        case 5 : return 0xE;
        case 6 : return 0x2;
        case 7 : return 0xE;
        case 8 : return 0x7;
        case 9 : return 0xA;
        case 10 : return 0x1;
        case 11 : return 0xA;
        case 12 : return 0x7;
        case 13 : return 0xE;
        case 14 : return 0x2;
        case 15 : return 0xE;//8185C2C5
        case 16 : return 0x8;
        case 17 : return 0x1;
        case 18 : return 0x8;
        case 19 : return 0x5;
        case 20 : return 0xC;
        case 21 : return 0x2;
        case 22 : return 0xC;
        case 23 : return 0x5;
        case 24 : return 0x8;
        case 25 : return 0x1;
        case 26 : return 0x8;
        case 27 : return 0x5;
        case 28 : return 0xC;
        case 29 : return 0x2;
        case 30 : return 0xC;
        case 31 : return 0x5;
        default :break;
    }
}
char pattern1(char index)
{
    switch (index)
    {
        case 0 : return 0x7;
        case 1 : return 0xB;
        case 2 : return 0x1;
        case 3 : return 0x9;
        case 4 : return 0x5;
        case 5 : return 0xD;
        case 6 : return 0x3;
        case 7 : return 0xF;
        default :break;
    }
}

void movement_pattern(t_entity * entity)
{
    TRISA = 0x00;
    
   char movement;
   char loop;
   switch (entity->pattern)
   {
       case '1' : movement = pattern1(entity->offset_movement);
       loop = 7;
       break;
       case '2' : movement = pattern2(entity->offset_movement);
       loop = 31;
       break;
       case '3' : movement= pattern3(entity->offset_movement); //Missile player
       loop = 1;
       break;
       case '4' : movement= pattern4(entity->offset_movement); //Missile ennemi 
       loop = 1;
       break;
       case '5' : movement= pattern5(entity->offset_movement);
       loop = 1;
       break;
       case '6' : movement= pattern6(entity->offset_movement);
       loop = 1;
       case '7' : movement= pattern7(entity->offset_movement);
       loop = 15;
       break;
       
   }
   // movement = *entity->pattern->movement & (0b1111 << (sizeof(entity->pattern->movement)*8 - (entity->offset_movement>>1)*4));
   //    movement = *entity->pattern & (0b1111 << ((entity->offset_movement>>1)*4));

   if((movement&0b0001) && (!entity->witness))
    {
         entity->witness=1;
    }
    else {
            entity->witness=0;
            entity->offset_movement += 1;
            if(entity->offset_movement > loop) entity->offset_movement=0;


        }
    if(!(movement&0b0010)) //If bit 1 is set to 0
    {
        if(!(movement&0b1110))
        {
            movement_down(entity,2); //000X : DOWN
        }
        else if(movement&0b0100) //bit 2 to 1
        {
            if(movement&0b1000) //110X : UP + RIGHT
            {
                movement_up(entity,2);
                movement_right(entity,2);
            }
            else movement_right(entity,3); //010X : RIGHT
        }
        else //100X : DOWN + RIGHT
        {
            movement_right(entity,2);
            movement_down(entity,2);
        }
    }
    else
    {
        if(!(movement&0b1100)) movement_up(entity,2); //001X : UP
        else if(movement&0b0100) //bit 2 to 1
        {
            if(movement&0b1000) //111X : UP + LEFT
            {
                movement_up(entity,2);
                movement_left(entity,2);
            }
            else movement_left(entity,3); //011X : LEFT
        }
        else //101X : DOWN + LEFT
        {
            movement_left(entity,2);
            movement_down(entity,2);
        }
    }
}

void movement_down(t_entity * object,char value)
{
    object->posy-=value;
  
}
void movement_up(t_entity * object,char value)
{
    object->posy+=value;
}
void movement_right(t_entity * object, char value)
{
    object->posx+=value;
}
void movement_left(t_entity * object, char value)
{
    object->posx-=value;
}




