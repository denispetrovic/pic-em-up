/* 
 * File:   movement.h
 * Author: Erwan
 *
 * Created on 3 d�cembre 2016, 22:15
 */

#ifndef MOVEMENT_H
#define	MOVEMENT_H

#include "entity.h"



void movement_down(t_entity * object,char value);
void movement_up(t_entity * object,char value);
void movement_right(t_entity * object,char value);
void movement_left(t_entity * object,char value);
char pattern1(char index);
char pattern2(char index);
char pattern3(char index);
char pattern4(char index);
char pattern5(char index);
char pattern6(char index);
char pattern7(char index);




void movement_pattern(t_entity * object);







/***
  ----  Directions ---

    000X : DOWN
    001X : UP
    010X : RIGHT
    011X : LEFT
    100X : DOWN + RIGHT
    101X : DOWN + LEFT
    110X : UP + RIGHT
    111X : UP + LEFT
***/

#ifdef	__cplusplus
extern "C" {
#endif




#ifdef	__cplusplus
}
#endif

#endif	/* MOVEMENT_H */

