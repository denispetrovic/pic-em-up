#include "menu.h"

// displays the main menu
void menu_loop(void)
{
	unsigned char string1[12] = { 'P', 'I', 'C', ' ', 'T', 'H', 'E', 'M', ' ', 'U', 'P', '\0'} ;
	unsigned char string2[11] = { 'R', 'E', '0', ' ', '-', ' ', 'P', 'L', 'A', 'Y', '\0'} ;
    unsigned char string3[12] = { 'R', 'E', '1', ' ', '-', ' ', 'H', 'E', 'L', 'P', '!', '\0'} ;
    unsigned char string4[14] = { 'R', 'E', '2', ' ', '-', ' ', 'C', 'R', 'E', 'D', 'I', 'T', 'S', '\0'} ;
    display_bmp_string(string1, 1, 0) ;
    display_bmp_string(string2, 1, 10) ;
    display_bmp_string(string3, 1, 20) ;
    display_bmp_string(string4, 1, 30) ;
    __delay_ms(80) ;
}

// displays help menu
void help(void)
{
	unsigned char string1[4] = { 'P', 'I', 'C', '\0'} ;
	unsigned char string2[5] = { 'T', 'H', 'E', 'M', '\0'} ;
	unsigned char string3[3] = { 'U', 'P', '\0' } ;
    display_bmp_string(string1, 10, 0) ;
    display_bmp_string(string2, 10, 10) ;
    display_bmp_string(string3, 10, 20) ;
    __delay_ms(80) ;
}

// displays credits
void credits(void)
{
	unsigned char string1[13] = { 'D', 'E', 'S', 'I', 'G', 'N', 'E', 'D', ' ', 'B', 'Y', ':', '\0'} ;
	unsigned char string2[7] = { 'A', 'R', 'N', 'A', 'U', 'D', '\0'} ;
	unsigned char string3[7] = { 'C', 'A', 'R', 'L', 'O', 'S', '\0'} ;
	unsigned char string4[6] = { 'D', 'E', 'N', 'I', 'S', '\0'} ;
    unsigned char string5[6] = { 'E', 'R', 'W', 'A', 'N', '\0'} ;
    unsigned char string6[8] = { 'F', 'L', 'O', 'R', 'I', 'A', 'N', '\0'} ;
    unsigned char string7[7] = { 'T', 'H', 'O', 'M', 'A', 'S', '\0'} ;
    display_bmp_string(string1, 10, 0) ;
    display_bmp_string(string2, 10, 10) ;
    display_bmp_string(string3, 10, 20) ;
    display_bmp_string(string4, 10, 30) ;
    display_bmp_string(string5, 10, 40) ;
    display_bmp_string(string6, 10, 50) ;
    display_bmp_string(string7, 10, 60) ;
    __delay_ms(80) ;
}

// displays end of game screen when victory
void gg_victory(void)
{
    unsigned char string[11] = { 'G', 'O', 'O', 'D', ' ', 'G', 'A', 'M', 'E', '!', '\0' } ;
    display_bmp_string(string, 10, 20) ;
    __delay_ms(80) ;
}

// displays end of game screen when defeat
void try_again(void)
{
    unsigned char string[10] = { 'G', 'A', 'M', 'E', ' ', 'O', 'V', 'E', 'R', '\0' } ;
    display_bmp_string(string, 10, 20) ;
    __delay_ms(80) ;
}