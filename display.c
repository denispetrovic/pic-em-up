#include "display.h"

char bitmap[1024] = {0};

const unsigned char Font3x6[65][3]={
0x00,0x00,0x00, /* Espace	0x20 */
0x00,0x5C,0x00, /* ! */
0x0C,0x00,0x0C, /* " */
0x7C,0x28,0x7C, /* # */
0x7C,0x44,0x7C, /* 0x */
0x24,0x10,0x48, /* % */
0x28,0x54,0x08, /* & */
0x00,0x0C,0x00, /* ' */
0x38,0x44,0x00, /* ( */
0x44,0x38,0x00, /* ) */
0x20,0x10,0x08, /* // */
0x10,0x38,0x10, /* + */
0x80,0x40,0x00, /* , */
0x10,0x10,0x10, /* - */
0x00,0x40,0x00, /* . */
0x20,0x10,0x08, /* / */
0x38,0x44,0x38, /* 0	0x30 */
0x00,0x7C,0x00, /* 1 */
0x64,0x54,0x48, /* 2 */
0x44,0x54,0x28, /* 3 */
0x1C,0x10,0x7C, /* 4 */
0x4C,0x54,0x24, /* 5 */
0x38,0x54,0x20, /* 6 */
0x04,0x74,0x0C, /* 7 */
0x28,0x54,0x28, /* 8 */
0x08,0x54,0x38, /* 9 */
0x00,0x50,0x00, /* : */
0x80,0x50,0x00, /* ; */
0x10,0x28,0x44, /* < */
0x28,0x28,0x28, /* = */
0x44,0x28,0x10, /* > */
0x04,0x54,0x08, /* ? */
0x38,0x4C,0x5C, /* @	0x40 */
0x78,0x14,0x78, /* A */
0x7C,0x54,0x28, /* B */
0x38,0x44,0x44, /* C */
0x7C,0x44,0x38, /* D */
0x7C,0x54,0x44, /* E */
0x7C,0x14,0x04, /* F */
0x38,0x44,0x34, /* G */
0x7C,0x10,0x7C, /* H */
0x00,0x7C,0x00, /* I */
0x20,0x40,0x3C, /* J */
0x7C,0x10,0x6C, /* K */
0x7C,0x40,0x40, /* L */
0x7C,0x08,0x7C, /* M */
0x7C,0x04,0x7C, /* N */
0x7C,0x44,0x7C, /* O */
0x7C,0x14,0x08, /* P	0x50 */
0x38,0x44,0x78, /* Q */
0x7C,0x14,0x68, /* R */
0x48,0x54,0x24, /* S */
0x04,0x7C,0x04, /* T */
0x7C,0x40,0x7C, /* U */
0x3C,0x40,0x3C, /* V */
0x7C,0x20,0x7C, /* W */
0x6C,0x10,0x6C, /* X */
0x1C,0x60,0x1C, /* Y */
0x64,0x54,0x4C, /* Z */
0x7C,0x44,0x00, /* [ */
0x08,0x10,0x20, /* \ */
0x44,0x7C,0x00, /* ] */
0x08,0x04,0x08, /* ^ */
0x80,0x80,0x80, /* _ */
0x04,0x08,0x00 /* `	0x60 */
	};

const char blob_croix[2] = {0b10010110, 0b01101001};
const char blob_window[3] = {0b11111001, 0b10011001, 0b11111111};
//const char blob_me[3] = {0x30, 0xCF, 0xFF};
const char blob_me[26] = {0b00110000,0b00000001,0b11000000,0b00001111,0b00000000,0b01111000,0b00000011,0b11000000,0b00011110,0b00000001,0b11111111,0b11011111,0b11111111,0b11111111,0b11111011,0b11111111,0b10001111,0b00000000,0b01111000,0b00000011,0b11000000,0b00011110,0b00000000,0b11100000,0b00000110,0b00000000};
const char blob_en1[21] = {0b00000000,0b01100000,0b00000111,0b11101111,0b11111111,0b10111111,0b11111110,0b00000001,0b11111100,0b00000011,0b11110000,0b00001111,0b11000000,0b01111111,0b11111111,0b11111011,0b11111111,0b11100000,0b00011111,0b10000000,0b00011000};
//const char blob_en1[3] = {0x77, 0xFF, 0x50};
const char blob_en2[18] = {0xFE,0xFC,0xFC,0xFC,0xF8,0xF8,0xF8,0xF8,0xF8,0x90,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xF0,0x9F};
const char blob_en3[36] = {0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFC,0xF8,0xF8,0x00,0x80,0xC0,0xE0,0xF8,0xFC,0xFE,0xFC,0xFC,0xFC,0xF8,0xF8,0xF8,0xF8,0xF8,0x78,0x30,0x00,0x00,0x00,0x00,0x00,0x38,0x7C};
const char blob_boss[124] = {0xFF,0xFF,0xFC,0xF8,0xF0,0xF0,0xE0,0xC0,0xC0,0x80,0x80,0x80,0x00,0x00,0x00,0x00,0x00,0x00,0x80,0x80,0x80,0xC0,0xC0,0xE0,0xE0,0xF0,0xF0,0xF8,0xF8,0xFC,0xFC,0xFC,0xFE,0xFF,0xFF,0xFF,0xFE,0xFE,0xFC,0xFC,0xF8,0xF0,0xC0,0x00,0x00,0x00,0x00,0x00,0x80,0x80,0xC0,0xC0,0xC0,0xC0,0xE0,0xE0,0xE0,0xE0,0xE0,0xE0,0xE0,0xF0,0xC7,0x07,0x07,0x07,0x07,0x07,0x07,0x07,0x07,0x07,0x0F,0x0F,0x1F,0x1F,0x1F,0x1F,0x1F,0x0F,0x0F,0x07,0x01,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x10,0x10,0x18,0x3F};
//const char blob_tir1[1] = {0x7F};
const char blob_tir1[1] = {0xFF};

const char blob_tir2[3] = {0xCF,0x1F,0xCF};

const struct sprite sprites[SPRITE_NB] = {
    {
        4, 4, &blob_croix
    },
    {
       4, 5, &blob_window
    },
    {
        13, 16, blob_me
        //6, 4, blob_me
    },
    {
        14, 14, blob_en1
        //5, 4, blob_en1
    },
    {
        18, 16, blob_en2
    },
    {
        18, 32, blob_en3
    },
    {
        62, 32, blob_boss
    },
    {
        3, 2, blob_tir1
    },
    {
        3, 3, blob_tir2
    }
};

void display_bmp_fill(char col)
{
    char x,y;
    for(y=0; y < 64; y++)
    {
        for(x=0; x < 16; x++)
        {
            col?bitmap[y*16+x]=0xFF:bitmap[y*16+x]=0;
        }
    }
  //  display_glcd();
}

char display_bmp_pixel(char col, char pos_x, char pos_y)
{
    char collision = 0;

    if((pos_x >= 0 && pos_x < GLCD_HORIZONTAL)
            && (pos_y >= 0 && pos_y < GLCD_VERTICAL))
    {// If the position is within the bounds of the screen
        if(col){ // We set the bit
            // But first we check if there is a collision going on
            collision = bitmap[((pos_y>>3)<<7)+pos_x] & (0x01 << pos_y%8);
            
            bitmap[((pos_y>>3)<<7)+pos_x] |= (0x01 << pos_y%8);
        }
        else{ // We clear the bit
            bitmap[((pos_y>>3)<<7)+pos_x] &= ~(0x01 << (pos_y%8));
            // We don't care about the collision for a white pixel
        }
    }
    return collision;
}
char display_bmp_pixel_collision(char col, char pos_x, char pos_y)
{
    char collision = 0;

    if((pos_x >= 0 && pos_x < GLCD_HORIZONTAL)
            && (pos_y >= 0 && pos_y < GLCD_VERTICAL))
    {// If the position is within the bounds of the screen
        if(col){ // We set the bit
            // But first we check if there is a collision going on
            collision = bitmap[((pos_y>>3)<<7)+pos_x] & (0x01 << pos_y%8);
        }
    }
    return collision;
}

char display_bmp_sprite(char sprite, char pos_x, char pos_y)
{
    char collision = 0;
    char col;
    t_sprite* s = &sprites[sprite];
    
    char x,y;

    for(y = 0; y < s->length_vertical; y++){
        for (x = 0; x < s->length_horizontal; x++){
            //col = display_decode(s, x, y);
            
           col = s->blob[(y*s->length_horizontal+x)>>3] & (0x80 >> (y*s->length_horizontal+x)%8);
            collision |= display_bmp_pixel(col, pos_x+x, pos_y+y);
        }
    }
    return collision;
}

char display_bmp_sprite_collision(char sprite, char pos_x, char pos_y)
{
    char collision = 0;
    char col;
    t_sprite* s = &sprites[sprite];
    
    char x,y;

    for(y = 0; y < s->length_vertical; y++){
        for (x = 0; x < s->length_horizontal; x++){            
            collision |= display_bmp_pixel_collision(col, pos_x+x, pos_y+y);
        }
    }
    return collision;
}


void display_glcd(void)
{
    unsigned char cs;
    int ptr=0;	// pointeur display

    unsigned char i, j;

        // Boucle sur les pages verticales (decalage de 2*8=16 pixel en haut)
	for(i = 0; i < 8; i++)
	{
		// Boucle sur les deux pages horizontales
		for(cs=0;cs<=1;cs++)
		{
			GLCD_RS=0;						// Envoi instruction
            glcd_WriteByte(cs, 0x40);		// Adresse horizontal 0
            glcd_WriteByte(cs, i | 0xB8);	// Adresse de la page 0+i
            GLCD_RS=1;						// Envoi de donnee

            // Boucle sur les octets horizontaux
			for(j = 0; j < 64; ++j)
			{
				//glcd_WriteByte(cs, TopoVector[ptr]);  // Envoi de l'octet de donnee
				glcd_WriteByte(cs, bitmap[ptr]);
				ptr+=1;
			}
            
		}
	}

     GLCD_CS1=0;
     GLCD_CS2=0;
}

void display_bmp_letter(char letter, char pos_x, char pos_y)
{
    char x,y;
    char color;
    letter -= 32;
    
    for(x=0; x<3;x++){
        for(y=0; y<5; y++){
            color = Font3x6[letter][x] & (0x4<<y);
            display_bmp_pixel(color, pos_x+x, pos_y+y);
        }
    }
}

void display_bmp_string(char* string, char pos_x, char pos_y)
{
    char i = 0;
    char x = pos_x;
    char y = pos_y;
    while(string[i]){
        if(string[i]=='\n'){
            y += 6;
            x = pos_x;
        }
        else{
            display_bmp_letter(string[i], x, y);
            x += 4;
        }
        i++;
    }

}

char display_decode(t_sprite* s, int x, int y){
    int test=0;
    int ytemp=y;
 
    if(y>(s->length_vertical>>1))
    {
        ytemp=(s->length_vertical>>1) - (y%((s->length_vertical)>>1));

    }
    y = ytemp;
 
    test=s->blob[(y*(s->length_horizontal>>2)+x)>>3]&(0x80 >> (y*s->length_horizontal+x)%8);
 
    if(test)
    {
        return '0';
    }else
    {
        return '1';
    }
}