
#include "game.h"
#include "init.h"
//#include <timers.h>



 char game_loop(t_game *game_t){
     
 	char i,j;
 	char missile_counter=0,free_space;
    char antispam = 0;
int cycle_counter=0;	
    t_entity player;
    player.posx = 5;
    player.posy = 5;
    player.sprite = 2;
    player.type = 3;
    player.state = 1;
    char lim_missile = 5;
    t_entity missiles[20];
    
    t_entity ennemy;
    t_entity ennemy2;
    //createScriptStruct(game_t);
    entity_randomize(&ennemy);
    entity_randomize2(&ennemy2);
   // entity_randomize(game_t->entities[1]);
   // entity_randomize(game_t->entities[2]);
    game_t->state = 1;
    game_t->score='0';
    char test = 1;
    //createGameStruct(game);
 	while (game_t->state) {//While the player is alive
        
 		cycle_counter++;
        if(cycle_counter%10) antispam=0;
        
        //If the counter reaches the cycle number defined in the script, we display the enemies
        /*if(cycle_counter == game_t->script[enemy_counter].cycle_number) {
            
            free_space=check_if_free_space(game_t->entities);
            if(free_space!=-1) {//If there is enough space in memory
                game_t->entities[free_space]=&game_t->script[enemy_counter].entity;
                for (i=0;i<(game_t->script[enemy_counter].nb_of_enemies_in_group-1);i++){
//                    if(i==0) {//We only do it once
//                        temporary_counter=enemy_counter;
//                    }
                    enemy_counter++;
                    //game->script[enemy_counter]->entity->offset_movement=1+i;//We increase the offset for the rest of the enemies of the group
                    free_space=check_if_free_space(game_t->entities);
                    if(free_space!=-1){
                        game_t->entities[free_space]=&game_t->script[temporary_counter].entity;
                    } else {
                        break;
                    }                    
                }
                enemy_counter=0;
            }
        }*/
// 		//We check all the entities and if one is dead we make it point to NULL.
// 		mark_enemy_as_dead()
//      
 		//Movement of other entities (enemies, bullets, and walls)
        
        if(cycle_counter%4)
        {
            movement_pattern(&ennemy);
            movement_pattern(&ennemy2);
        }
        
        
        
        for(i=0;i<missile_counter;i++)
        {
            movement_pattern(&missiles[i]);
            display_bmp_sprite(missiles[i].sprite,missiles[i].posx,missiles[i].posy);
            if(missiles[i].posx > 127) 
            {
                for(j=i;j<missile_counter-1;j++)
                {
                    missiles[j] = missiles[j+1];
                }
                missile_counter--;
                i--;
                
            }
        }
        /*
 		for (i=0;i<3+missile_counter;i++) {
            
 			switch (game_t->entities[i]->type) {
 				case 0://We could consider that 0 is an ennemy
 					if (cycle_counter%3){
 						movement_pattern(game_t->entities[i]);
                        if(display_bmp_sprite(game_t->entities[i]->sprite,game_t->entities[i]->posx,game_t->entities[i]->posy) || (game_t->entities[i]->posx>128) || (game_t->entities[i]->posy>64) || (game_t->entities[i]->posx<0) || (game_t->entities[i]->posy<0) )
                        {
                           // game_t->entities[i]->state=0;
                        }
 					}
 					break;
 				case 1://1 is Bullets
 					movement_pattern(game_t->entities[i]);
                    display_bmp_sprite(game_t->entities[i]->sprite,game_t->entities[i]->posx,game_t->entities[i]->posy);
                    if(game_t->entities[i]->posx >= 128)
                    {
                        game_t->entities[i]->state=0;
                    }
 					break;
 				// case 2://2 is Walls
 				// 	if (cycle_counter==3){
 				// 		movement_pattern(game->entities[i]);
 				// 	}
 				// 	break;
 			}
 		}
        for(i=0;i<3+missile_counter;i++)
        {
           if(game_t->entities[i]->state==0)
           {
               if(game_t->entities[i]->type==0) entity_randomize(game_t->entities[i]);
               else{

               for(j=i;j<missile_counter-1;j++)
               {
                   game_t->entities[j]=game_t->entities[j+i];
               }
               missile_counter--;
               i--;
               }
               
           }*/
        

          //Read inputs
        if(PORTAbits.RA0 == 1)
        {
            movement_right(&player, 2);
        }
        else if(PORTAbits.RA1 == 1)
        {
            movement_up(&player, 2);
        }
        else if(PORTAbits.RA2 == 1)
        {
            movement_down(&player, 2);
        }
        else if(PORTAbits.RA3 == 1)
        {
            movement_left(&player, 2);
        }
        display_bmp_sprite(2,player.posx,player.posy);
        
        if((PORTEbits.RE0==1) && (missile_counter < lim_missile) && (!antispam))
        {
            missiles[missile_counter].pattern = '3';
            missiles[missile_counter].sprite = 7;
            missiles[missile_counter].posx = player.posx;
            missiles[missile_counter].posy = player.posy;
            missile_counter++;
            if(game_t->score > '5' && missile_counter < 20)
            {
            missiles[missile_counter].pattern = '3';
            missiles[missile_counter].sprite = 7;
            missiles[missile_counter].posx = player.posx;
            missiles[missile_counter].posy = player.posy+16;
            missile_counter++;
            }
            antispam = 1;
        }
        
        if(display_bmp_sprite(ennemy.sprite,ennemy.posx,ennemy.posy))
        {
            test = 1;
            display_bmp_fill(0);
            if(test)
            {
                game_t->score++;
                test=0;
            }
            entity_randomize(&ennemy);
        }
        if(display_bmp_sprite(ennemy2.sprite,ennemy2.posx,ennemy2.posy))
        {
            test = 1;
            display_bmp_fill(0);
            if(test)
            {
                game_t->score++;
                test=0;
            }
            entity_randomize2(&ennemy2);
        }
        
        if(cycle_counter==10000) cycle_counter = 0;
		
 		//Bullet collision
 		//If player bullet hit ennemy, increase score and mark ennemy as dead
		
 		//Shooting
 		

 		//Show on screen
        display_bmp_letter(game_t->score,100,50);
        if(game_t->score > '9')
        {
            game_t->score = '0';
            if(lim_missile<20) lim_missile++;
        }
        display_glcd();
 		__delay_ms(80); //Wait 10 ms
        display_bmp_fill(0);
        __delay_ms(10);

 	}
    
 	return 0; // Means dead or no more lives
 }

 void createScriptStruct(t_game *game_t) {
    int i;
    game_t->state=1;//Alive
    game_t->score=0;
    game_t->movement='n';//No movement at first
    
  /*  for (i=0;i<MAX_ENTITIES_ON_SCREEN;i++){
       // game_t->entities[i]=0;//We point to a random but well defined location so that it's like a "NULL"
    }*/
    for (i=0;i<TOTAL_ENEMIES_AND_WALLS;i++){
        game_t->script[i].cycle_number=1000+i*1000;
        game_t->script[i].nb_of_enemies_in_group=4;
        game_t->script[i].entity->type=0;//Enemies
        game_t->script[i].entity->state=1*i;//The number of lives of the enemies. It gets progressively harder
        game_t->script[i].entity->sprite=3;//To define
        if(i%2) {
            game_t->script[i].entity->pattern=7;
            game_t->script[i].entity->posx=i%20;
            game_t->script[i].entity->posy=30+(i%10);
            game_t->script[i].entity->sprite=4;//To define

        } else if(i%3) {
            game_t->script[i].entity->pattern=1;
            game_t->script[i].entity->posx=50+i%9;
            game_t->script[i].entity->posy=i%35;
            game_t->script[i].entity->sprite=5;//To define

        } else if(i%5) {
            game_t->script[i].entity->pattern=2;
            game_t->script[i].entity->posx=70+i%5;
            game_t->script[i].entity->posy=40+i%20;
        }
        game_t->script[i].entity->offset_movement=0;
    }
}
