
#ifndef SPRITE_H
#define	SPRITE_H


#define GLCD_HORIZONTAL 128
#define GLCD_VERTICAL 64
#define BITMAP_SIZE 1024 // GLCD pixels size / 8

#define SPRITE_NB 9 // How many sprites are defined

#define SPRITE_CROSS 0
#define SPRITE_WINDOW 1
 
#define SPRITE_ME 2
#define SPRITE_EN1 3
#define SPRITE_EN2 4
#define SPRITE_EN3 5
#define SPRITE_BOSS 6
 
#define SPRITE_TIR1 7
#define  SPRITE_TIR2 8

#include "glcd.h"

// Stored in memory, will be shown on the GLCD once per game cycle
char bitmap[1024];

// All the necessary information about a sprite
typedef struct sprite
{
    char length_horizontal;
    char length_vertical;
    char *blob; // index of RLE-encoded picture
}t_sprite;

//const struct sprite sprites[SPRITE_NB];

// Data about the letters and numbers
typedef struct alphabet
{
    
}t_alphabet;

//const t_sprite sprite_croix;
//const t_sprite sprite_window;
// 
//const t_sprite sprite_me;
//const t_sprite sprite_en1;
//const t_sprite sprite_en2;
//const t_sprite sprite_en3;
//const t_sprite sprite_boss;
// 
//const t_sprite sprite_tir1;
//const t_sprite sprite_tir2;

// Displays the bitmap on the glcd screen
void display_glcd(void);

// Fills the whole bitmap with the color col
void display_bmp_fill(char col);

// Puts a pixel on the bitmap 
// Pixel of the color col at (pos_x; pos_y)
char display_bmp_pixel(char col, char pos_x, char pos_y);
char display_bmp_pixel_collision(char col, char pos_x, char pos_y);

// Decodes the sprite sprite and puts it on the bitmap at the position x;y
char display_bmp_sprite(char sprite, char pos_x, char pos_y);
char display_bmp_sprite_collision(char sprite, char pos_x, char pos_y);

void display_bmp_string(char* string, char pos_x, char pos_y);

void display_bmp_letter(char letter, char pos_x, char pos_y);

char display_decode(t_sprite* s, int x, int y);

// Displays the entity at the correct position on the bitmap
// TODO move to entity.h file
//void display_bmp_entity(t_entity* entity);

#endif